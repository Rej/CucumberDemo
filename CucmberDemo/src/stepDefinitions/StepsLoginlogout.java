package stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepsLoginlogout {
	
	WebDriver Driver=new FirefoxDriver();
	
	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
	    Driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);		
		Driver.get("http://opensource.demo.orangehrm.com/");
	}

	@When("^Enetr user valid id and password$")
	public void enetr_user_valid_id_and_password() throws Throwable {
		Driver.findElement(By.id("txtUsername")).sendKeys("Admin");
		Driver.findElement(By.id("txtPassword")).sendKeys("admin");
	}

	@When("^Click login button$")
	public void click_login_button() throws Throwable {
		Driver.findElement(By.id("btnLogin")).click();
	}

	@Then("^Home page should get Loaded Sucessfully$")
	public void home_page_should_get_Loaded_Sucessfully() throws Throwable {
		Driver.findElement(By.id("welcome")).isDisplayed();
	}

	@When("^User click logout$")
	public void user_click_logout() throws Throwable {
		Driver.findElement(By.id("welcome")).click();
		Driver.findElement(By.linkText("Logout")).click();
	}

	@Then("^User shoul be logout sucessfully$")
	public void user_shoul_be_logout_sucessfully() throws Throwable {
		Driver.quit();
	}

	@When("^Enetr user invalid id and password$")
	public void enetr_user_invalid_id_and_password() throws Throwable {
		Driver.findElement(By.id("txtUsername")).sendKeys("Admin");
		Driver.findElement(By.id("txtPassword")).sendKeys("addsdfmin");
	}

	@Then("^Error message should be displayed$")
	public void error_message_should_be_displayed() throws Throwable {
		Driver.findElement(By.id("spanMessage")).isDisplayed();
	}
}
